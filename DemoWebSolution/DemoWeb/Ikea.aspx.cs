﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class Ikea : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ( ! IsPostBack)
            {
                txtNom.Text = "ex : Michel";
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            lblMessage.Text = "Bonjour " + txtNom.Text;
        }

        protected void btnItalique_Click(object sender, EventArgs e)
        {
            lblMessage.Font.Italic = true;
            lblMessage.ForeColor = System.Drawing.Color.HotPink;
        }
    }
}