﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class TestBdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // implémente un contexte :
            EntrepriseEntities context = new EntrepriseEntities();

            Personne pers = new Personne();
            pers.Nom = "Bigard";
            pers.Prenom = "JM";

            context.Personnes.Add(pers);

            context.SaveChanges();

            // parcours des personnes :
            foreach(Personne p in context.Personnes)
            {
                Response.Write(p.Nom);
            }
           
        }
    }
}