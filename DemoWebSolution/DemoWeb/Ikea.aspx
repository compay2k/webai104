﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ikea.aspx.cs" Inherits="DemoWeb.Ikea" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    

        <div>
            <asp:Button ID="btnItalique" OnClick="btnItalique_Click" runat="server" Text="Italique" />
            <br />
            <asp:Label ID="lblNom" EnableViewState="false" runat="server" Text="Votre nom : "></asp:Label>
            <asp:TextBox ID="txtNom" runat="server"></asp:TextBox>
            <asp:Button ID="btnOk" runat="server" OnClick="btnOk_Click" Text="OK" />
            <asp:Label ID="lblMessage" runat="server" Text="plop"></asp:Label>
        </div>
    </form>
</body>
</html>
