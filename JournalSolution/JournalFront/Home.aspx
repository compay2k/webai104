﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Arche.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="JournalFront.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="contenuPpal" runat="server">

    <section>
        <header>
            <h2>Sport</h2>
        </header>
        <nav>
            <a href="#">Babyfoot</a>
            <a href="#">Tennis</a>
            <a href="#">Danse</a>
        </nav>
        <div class="listeArticles">
            <article>
                <img src="https://img.lemde.fr/2018/10/12/313/0/3746/1873/312/156/60/0/fe05f4d_FTa3ocTFfYjVp-qLP320XzW9.jpg" alt="duel remporté" />
                <h3>Duel au sommet sans surprise</h3>
                <p>Une fois de plus, le sort a été conforme à l'implacable logique du sport...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="duel remporté" />
                <h3>Duel au sommet sans surprise</h3>
                <p>Une fois de plus, le sort a été conforme à l'implacable logique du sport...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="duel remporté" />
                <h3>Duel au sommet sans surprise</h3>
                <p>Une fois de plus, le sort a été conforme à l'implacable logique du sport...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="duel remporté" />
                <h3>Duel au sommet sans surprise</h3>
                <p>Une fois de plus, le sort a été conforme à l'implacable logique du sport...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="duel remporté" />
                <h3>Duel au sommet sans surprise</h3>
                <p>Une fois de plus, le sort a été conforme à l'implacable logique du sport...</p>
            </article>
        </div>
        <footer><a href="#">Tout le sport...</a></footer>
    </section>
    <section>
        <header>
            <h2>Dauphins</h2>
        </header>
        <nav>
            <a href="#">Flipper</a>
            <a href="#">Communication</a>
            <a href="#">Natation</a>
        </nav>
        <div class="listeArticles">
            <article>
                <img src="images/logo.png" alt="flipper crack" />
                <h3>Flipper : La rechute</h3>
                <p>C'est danc in état préoccupant que les authorités maritimes ont retrouvé Flipper, rattrappé par ses vieux démons...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="flipper crack" />
                <h3>Flipper : La rechute</h3>
                <p>C'est danc in état préoccupant que les authorités maritimes ont retrouvé Flipper, rattrappé par ses vieux démons...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="flipper crack" />
                <h3>Flipper : La rechute</h3>
                <p>C'est danc in état préoccupant que les authorités maritimes ont retrouvé Flipper, rattrappé par ses vieux démons...</p>
            </article>
            <article>
                <img src="images/logo.png" alt="flipper crack" />
                <h3>Flipper : La rechute</h3>
                <p>C'est danc in état préoccupant que les authorités maritimes ont retrouvé Flipper, rattrappé par ses vieux démons...</p>
            </article>
        </div>
        <footer><a href="#">Tous les dauphins...</a></footer>
    </section>

</asp:Content>
